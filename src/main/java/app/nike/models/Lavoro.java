package app.nike.models;

public class Lavoro {
	private long id;
	private String nomeAzienda;
	private String data;

	private String tipo;
	private String contratto;
	private int livello = 0;
	private long ral = 0;
	private long tariffaGiornalieraNetta = 0;
	private String specificaContratto;
	private String nomeReferente;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNomeAzienda() {
		return nomeAzienda;
	}

	public void setNomeAzienda(String nomeAzienda) {
		this.nomeAzienda = nomeAzienda;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getContratto() {
		return contratto;
	}

	public void setContratto(String contratto) {
		this.contratto = contratto;
	}

	public int getLivello() {
		return livello;
	}

	public void setLivello(int livello) {
		this.livello = livello;
	}

	public long getRal() {
		return ral;
	}

	public void setRal(long ral) {
		this.ral = ral;
	}

	public long getTariffaGiornalieraNetta() {
		return tariffaGiornalieraNetta;
	}

	public void setTariffaGiornalieraNetta(long tariffaGiornalieraNetta) {
		this.tariffaGiornalieraNetta = tariffaGiornalieraNetta;
	}

	public String getSpecificaContratto() {
		return specificaContratto;
	}

	public void setSpecificaContratto(String specificaContratto) {
		this.specificaContratto = specificaContratto;
	}

	public String getNomeReferente() {
		return nomeReferente;
	}

	public void setNomeReferente(String nomeReferente) {
		this.nomeReferente = nomeReferente;
	}

		
}
