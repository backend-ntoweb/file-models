package app.nike.models;

public class Certificazione {

	private long id;

	private String nomeCertificazione;
	
	private String scadenzaCertificazione;
	
	private Persona persona;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNomeCertificazione() {
		return nomeCertificazione;
	}

	public void setNomeCertificazione(String nomeCertificazione) {
		this.nomeCertificazione = nomeCertificazione;
	}

	public String getScadenzaCertificazione() {
		return scadenzaCertificazione;
	}

	public void setScadenzaCertificazione(String scadenzaCertificazione) {
		this.scadenzaCertificazione = scadenzaCertificazione;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}
}
