package app.nike.models;

import java.util.List;


public class Domanda {

	private long id;

	private String domandaTest;
	
	private List<Risposta> risposte;
	
	private Test test;

	public Domanda() {
		super();
	}

	public Domanda(String domandaTest, List<Risposta> risposte, Test test) {
		super();
		this.domandaTest = domandaTest;
		this.risposte = risposte;
		this.test = test;
	}

	public long getId() {
		return id;
	}

	public String getDomandaTest() {
		return domandaTest;
	}

	public void setDomandaTest(String domandaTest) {
		this.domandaTest = domandaTest;
	}

	public List<Risposta> getRisposte() {
		return risposte;
	}

	public void setRisposte(List<Risposta> risposte) {
		this.risposte = risposte;
	}

	public Test getTest() {
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}

	
	
}
