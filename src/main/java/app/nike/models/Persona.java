package app.nike.models;

import java.util.List;


public class Persona {
	
	private String codiceFiscale;
	
	private String nome;
	
	private String cognome;
	
	private String email;
	
	private String dataNascita;
	
	private String luogoNascita;
	
	private String cittadinanza;
	
	private String residenza;
	
	private String capResidenza;
	
	private String domicilio;
	
	private String capDomicilio;
	
	private String recapitoMobile;
	
	private String recapitoFisso;
	
	private boolean categoriaProtetta;
	
	private boolean candanneCivili;
	
	private String canale;
	
	private Diploma diploma;
	
	private List<Laurea> laurea;
	
	private List<Lavoro> lavoro;
	
	private List<Certificazione> certificazione;
	
	private boolean privacy=false;

	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(String dataNascita) {
		this.dataNascita = dataNascita;
	}

	public String getLuogoNascita() {
		return luogoNascita;
	}

	public void setLuogoNascita(String luogoNascita) {
		this.luogoNascita = luogoNascita;
	}

	public String getCittadinanza() {
		return cittadinanza;
	}

	public void setCittadinanza(String cittadinanza) {
		this.cittadinanza = cittadinanza;
	}

	public String getResidenza() {
		return residenza;
	}

	public void setResidenza(String residenza) {
		this.residenza = residenza;
	}

	public String getCapResidenza() {
		return capResidenza;
	}

	public void setCapResidenza(String capResidenza) {
		this.capResidenza = capResidenza;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getCapDomicilio() {
		return capDomicilio;
	}

	public void setCapDomicilio(String capDomicilio) {
		this.capDomicilio = capDomicilio;
	}

	public String getRecapitoMobile() {
		return recapitoMobile;
	}

	public void setRecapitoMobile(String recapitoMobile) {
		this.recapitoMobile = recapitoMobile;
	}

	public String getRecapitoFisso() {
		return recapitoFisso;
	}

	public void setRecapitoFisso(String recapitoFisso) {
		this.recapitoFisso = recapitoFisso;
	}

	public boolean isCategoriaProtetta() {
		return categoriaProtetta;
	}

	public void setCategoriaProtetta(boolean categoriaProtetta) {
		this.categoriaProtetta = categoriaProtetta;
	}

	public boolean isCandanneCivili() {
		return candanneCivili;
	}

	public void setCandanneCivili(boolean candanneCivili) {
		this.candanneCivili = candanneCivili;
	}

	public String getCanale() {
		return canale;
	}

	public void setCanale(String canale) {
		this.canale = canale;
	}

	public Diploma getDiploma() {
		return diploma;
	}

	public void setDiploma(Diploma diploma) {
		this.diploma = diploma;
	}

	public List<Laurea> getLaurea() {
		return laurea;
	}

	public void setLaurea(List<Laurea> laurea) {
		this.laurea = laurea;
	}

	public List<Lavoro> getLavoro() {
		return lavoro;
	}

	public void setLavoro(List<Lavoro> lavoro) {
		this.lavoro = lavoro;
	}

	public List<Certificazione> getCertificazione() {
		return certificazione;
	}

	public void setCertificazione(List<Certificazione> certificazione) {
		this.certificazione = certificazione;
	}

	public boolean isPrivacy() {
		return privacy;
	}

	public void setPrivacy(boolean privacy) {
		this.privacy = privacy;
	}

	
}
