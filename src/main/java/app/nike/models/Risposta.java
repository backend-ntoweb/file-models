package app.nike.models;

public class Risposta {

	private long id;

	private String rispostaDomanda;
	
	private int valore;
	
	private Domanda domanda;

	public Risposta() {
		super();
	}

	public Risposta(String rispostaDomanda, Domanda domanda) {
		super();
		this.rispostaDomanda = rispostaDomanda;
		this.domanda = domanda;
	}

	public long getId() {
		return id;
	}
	
	public String getRispostaDomanda() {
		return rispostaDomanda;
	}

	public void setRispostaDomanda(String rispostaDomanda) {
		this.rispostaDomanda = rispostaDomanda;
	}

	public Domanda getDomanda() {
		return domanda;
	}

	public void setDomanda(Domanda domanda) {
		this.domanda = domanda;
	}	
	

	public int getValore() {
		return valore;
	}

	public void setValore(int valore) {
		this.valore = valore;
	}

	
	
}
