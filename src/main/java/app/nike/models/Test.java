package app.nike.models;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class Test {

	private long id;
 
	private String nameTest;
	
 
	private int punteggioTotale;
	
 
	private List<Domanda> domande;
	
	private boolean testAttivo=false;
	
 
	private String dataCaricamento;
	
	public Test() {
		super();
	}

	public Test(String nameTest, List<Domanda> domande) {
		super();
		this.nameTest = nameTest;
		this.domande= domande;
	}

	public long getId() {
		return id;
	}
	
	

	public int getPunteggioTotale() {
		return punteggioTotale;
	}

	public void setPunteggioTotale(int punteggioTotale) {
		this.punteggioTotale = punteggioTotale;
	}

	public String getNameTest() {
		return nameTest;
	}

	public void setNameTest(String nameTest) {
		this.nameTest = nameTest;
	}

	public List<Domanda> getDomande() {
		return domande;
	}

	public void setDomande(List<Domanda> domande) {
		this.domande = domande;
	}

	public boolean isTestAttivo() {
		return testAttivo;
	}

	public void setTestAttivo(boolean selected) {
		this.testAttivo = selected;
	}
	

	public String getDataCaricamento() {
		return dataCaricamento;
	}

	
	public void setDataCaricamento() {
		Calendar c=Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);		
		this.dataCaricamento = c.getTime().toString();
	}

	

	
	
}
