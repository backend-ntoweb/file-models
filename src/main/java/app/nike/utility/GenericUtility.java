package app.nike.utility;

import java.util.Random;

import app.nike.models.Email;

public class GenericUtility {
	//EmailBuilder
			private static String corpoRegistrazione ="Ciao %s %s\n\nLe tue credenziali sono\n\nusername: %s\npassword: %s\n\n"
					+ "puoi decidere di cambiare le credenziali al primo accesso, Buon Lavoro!";
			public static Email registrationEmail(String codiceFiscale, String nome, String cognome, String email, String password) {
				Email emailToSend = new Email();
				emailToSend.setDestinatario(email);
				emailToSend.setOggetto("Registrazione NikeWebConsulting");
				emailToSend.setCorpo(String.format(corpoRegistrazione,nome,cognome,codiceFiscale,password));
				return emailToSend;
			}
		//================================================
		//General Utility
		public static String randomPassword() {
			int leftLimit = 65; // letter 'A'
			int rightLimit = 90; // letter 'Z'
			int targetStringLength = 8;
			Random random = new Random();

			String generatedString = random.ints(leftLimit, rightLimit + 1).limit(targetStringLength)
					.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
			return generatedString;
		}
}
