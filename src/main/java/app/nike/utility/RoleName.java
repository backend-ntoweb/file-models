package app.nike.utility;

public interface RoleName {
	
	public final static String ROLE_ADMIN = "ROLE_ADMIN";
	
	public final static String ROLE_USER = "ROLE_USER";
	
	public final static String ROLE_PERSONA = "ROLE_PERSONA";
	
	public final static String ROLE_SUPERADMIN = "ROLE_SUPERADMIN";
}
